import { Component } from 'react';
import './style.css';

export default class Border extends Component {
  render() {
    return (
      <div className="Border">
        <h1>{this.props.title}</h1>
        <div>{this.props.children}</div>
      </div>
    );
  }
}