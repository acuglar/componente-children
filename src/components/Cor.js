import { render } from '@testing-library/react';
import { Component } from 'react';

export default class Cor extends Component {
  render() {
    return (
      <div style={{ color: this.props.color }}>
        {this.props.children}
      </div>
    )
  }
}

