import './App.css';
import Border from './components/Border'
import Cor from './components/Cor'
import Font from './components/Font'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Border title="props Title">
          <div>props Children</div>
          <Border title="Primeiro teste">
            <Cor color="green" >
              <div>Primeiro componente</div>
            </Cor>
          </Border>
          <Border title="Segundo teste">
            <Font fontWeight="bold" >
              <div>Segundo componente</div>
            </Font>
          </Border>
        </Border>
      </header>
    </div>
  );
}

export default App;
