import { Component } from 'react';

export default class Font extends Component {
  render() {
    return (
      <div style={{ fontWeight: this.props.fontWeight }}>
        {this.props.children}
      </div>
    )
  }
}




